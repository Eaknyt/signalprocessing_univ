Signal processing - Univ
========================

Just practical work about signal processing.


Build
-----

Put all files in a folder named `source` (or choose another name).

```
cd source

bash build.sh
```

This script creates a folder named `build` beside your source folder, executes CMake then make.

CMake temporary files are put in `build/generated`.
Binaries are put in `build/install`.


Run
---

```
bash build.sh <targetName>
```

