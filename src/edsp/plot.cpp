#include "plot.h"

#include <algorithm>
#include <assert.h>
#include <fstream>
#include <iostream>


namespace {

// Classes
struct PlotCurve
{
    edsp::DoubleVector points;
    edsp::DoubleVector values;
};

struct PlotHighlight
{
    double from = 0.;
    double to = 0.;
    std::string color = "#808080";
};

// Constants
const char plotErrorMsg[] = "plot() > points and values have not the same size";
const char plotHighlightErrorMsg[] = "plotHighlight() > 'from' must be greater than 'to'";

// Variables
std::string m_outputDirPath;

std::string m_filename;

std::string m_title;

std::string m_xLabel = "x";
std::string m_yLabel = "y";

double m_xFrom = 0;
double m_xTo = 0;
double m_yFrom = 0;
double m_yTo = 0;

double m_xFromEff = 0;
double m_xToEff = 0;
double m_yFromEff = 0;
double m_yToEff = 0;

std::vector<PlotCurve> m_curves;
std::vector<std::string> m_legends;
std::vector<PlotHighlight> m_highlights;

std::vector<std::string> m_palette = {
    // http://godsnotwheregodsnot.blogspot.fr/2012/09/color-distribution-methodology.html
    "#006FA6", "#FF4A46", "#FF34FF", "#1CE6FF", "#008941",
    "#000000", "#A30059", "#7A4900", "#0000A6", "#B79762",
    "#004D43", "#8FB0FF", "#997D87", "#5A0007", "#809693",
    "#1B4400", "#4FC601", "#3B5DFF", "#4A3B53", "#FF2F80",
    "#61615A", "#BA0900", "#6B7900", "#00C2A0", "#FFAA92",
    "#FF90C9", "#B903AA", "#D16100", "#000035", "#7B4F4B",
    "#A1C299", "#300018", "#0AA6D8", "#013349", "#00846F",
    "#372101", "#FFB500", "#A079BF", "#CC0744", "#001E09",
    "#00489C", "#6F0062", "#0CBD66", "#EEC3FF", "#456D75",
    "#B77B68", "#7A87A1", "#788D66", "#885578", "#FAD09F",
    "#FF8A9A", "#D157A0", "#BEC459", "#456648", "#0086ED",
    "#886F4C", "#34362D", "#B4A8BD", "#00A6AA", "#452C2C",
    "#636375", "#A3C8C9", "#FF913F", "#938A81", "#575329",
    "#00FECF", "#B05B6F", "#8CD0FF", "#3B9700", "#04F757",
    "#C8A1A1", "#1E6E00", "#7900D7", "#A77500", "#6367A9",
    "#A05837", "#6B002C", "#772600", "#D790FF", "#9B9700",
    "#549E79", "#201625", "#72418F", "#BC23FF", "#99ADC0",
    "#3A2465", "#922329", "#5B4534", "#404E55", "#0089A3",
    "#CB7E98", "#A4E804", "#324E72", "#6A3A4C",
};


////////////////////////// Helpers //////////////////////////

const std::string datFilePath(int i = 0)
{
    return m_outputDirPath + m_filename + "_" + std::to_string(i) + ".dat";
}

const std::string gnuplotFilePath()
{
    return m_outputDirPath + m_filename + ".gnuplot";
}

const std::string outputFilePath()
{
    return m_outputDirPath + m_filename + ".png";
}

void adjustRanges()
{
    // Set the ranges for drawing
    m_xFromEff = m_xFrom;
    m_xToEff = m_xTo;
    m_yFromEff = m_yFrom;
    m_yToEff = m_yTo;

    if (m_xFromEff == 0 && m_xToEff == 0) {
        std::vector<double> xMins;
        std::vector<double> xMaxs;

        for (const PlotCurve &curve : m_curves) {
            xMins.push_back(curve.points[0]);
            xMaxs.push_back(*(curve.points.end() - 1));
        }

        m_xFromEff = (*std::min_element(xMins.begin(), xMins.end()));
        m_xToEff = (*std::max_element(xMaxs.begin(), xMaxs.end()));
    }

    if (m_yFromEff == 0 && m_yToEff == 0) {
        std::vector<double> yMins;
        std::vector<double> yMaxs;

        for (const PlotCurve &curve : m_curves) {
            auto minmax = std::minmax_element(curve.values.begin(),
                                              curve.values.end());

            yMins.push_back(*minmax.first);
            yMaxs.push_back(*minmax.second);
        }

        m_yFromEff = (*std::min_element(yMins.begin(), yMins.end()));
        m_yToEff = (*std::max_element(yMaxs.begin(), yMaxs.end()));
    }
}

void writeGnuplotFile()
{
    std::string l_outputFilePath = outputFilePath();

    std::ofstream file;
    file.open(gnuplotFilePath());

    file << "set term pngcairo enhanced font 'arial,10' fontscale 1.0" << std::endl;
    file << "set output '" << l_outputFilePath << "'" << std::endl;

    file << "set grid lc rgb '#808080' lw 0.8" << std::endl;
    file << "set title '" << m_title << "'" << std::endl;
    file << "set xlabel '" << m_xLabel << "'" << std::endl;
    file << "set ylabel '" << m_yLabel << "'" << std::endl;
    file << "set xrange [" << m_xFromEff << ":" << m_xToEff << "]" << std::endl;
    file << "set yrange [" << m_yFromEff << ":" << m_yToEff << "]" << std::endl;
    file << "set key outside box height 2" << std::endl;

    // Highlight objects
    for (const PlotHighlight &highlight : m_highlights) {
        file << "set obj rect from " << highlight.from << ", " << 0 << " "
             << "to " << highlight.to << ", " << m_yToEff << " "
             << "fc rgb '" << highlight.color << "'"
             << std::endl;
    }

    // Curves
    file << "plot ";

    std::size_t curvesCount = m_curves.size();

    for (std::size_t i = 0; i < curvesCount; ++i) {
        std::string legend = m_legends[i];

        std::string rawLegend = legend.empty() ? "notitle"
                                               : "title '" + legend + "'";

        if (i > 0) {
            file << std::string(5, ' ');
        }

        std::string color = m_palette[i % m_palette.size()];

        file << "'" << datFilePath(i) << "' "
             << "using 1:2 w l " << rawLegend << " lt rgb '" << color << "' "
             << "linewidth 1.2";

        if (curvesCount > 1 && i < curvesCount - 1) {
            file << ",\\" << std::endl;
        }
    }

    file.close();
}

void writeDatFile(int i = 0)
{
    std::ofstream datFile;
    datFile.open(datFilePath(i));

    PlotCurve theCurve = m_curves[i];

    for (int i = 0; i < theCurve.values.size(); ++i) {
        double t = theCurve.points[i];
        double val = theCurve.values[i];

        datFile << t << " " << val << std::endl;
    }

    datFile.close();
}

void callGnuplot()
{
    std::string cmd = "gnuplot " + gnuplotFilePath();

    system(cmd.c_str());
}

void cleanGnuplotFiles()
{
    const char *cmd = "rm output/*.gnuplot output/*.dat";

    system(cmd);
}

} // anon namespace


////////////////////////// Implementations //////////////////////////

namespace edsp {

void plot(const DoubleVector &points, const DoubleVector &values)
{
    if (points.size() != values.size()) {
        std::cerr << plotErrorMsg << std::endl;
        return;
    }

    m_curves.clear();

    plotAgain(points, values);
}

void plotAgain(const DoubleVector &points, const DoubleVector &values)
{
    if (points.size() != values.size()) {
        std::cerr << plotErrorMsg << std::endl;
        return;
    }

    PlotCurve curve {points, values};
    m_curves.push_back(curve);

    m_legends.push_back("");

    adjustRanges();
}

void plotLegend(const std::string &legend, int i)
{
    assert (0 <= i && i < m_legends.size());

    std::string &theLegend = m_legends[i];

    if (theLegend != legend) {
        theLegend = legend;
    }
}

void plotTitle(const std::string &title)
{
    if (m_title != title) {
        m_title = title;
    }
}

void plotLabels(const std::string &xLabel, const std::string &yLabel)
{
    if (m_xLabel != xLabel) {
        m_xLabel = xLabel;
    }

    if (m_yLabel != yLabel) {
        m_yLabel = yLabel;
    }
}

void plotArea(double xFrom, double xTo, double yFrom, double yTo)
{
    // Check and assign
    if ( (m_xFrom != xFrom || m_xTo != xTo) && xFrom <= xTo) {
        m_xFrom = xFrom;
        m_xTo = xTo;
    }

    if ( (m_yFrom != yFrom || m_yTo != yTo) && yFrom <= yTo) {
        m_yFrom = yFrom;
        m_yTo = yTo;
    }

    adjustRanges();
}

void plotHighlight(double from, double to, const std::string &color)
{
    if (to < from) {
        std::cerr << plotHighlightErrorMsg << std::endl;
        return;
    }

    PlotHighlight highlight {from, to, color};

    m_highlights.push_back(highlight);
}

void plotResetHighlights()
{
    m_highlights.clear();
}

void plotDirectory(const std::string &path)
{
    static const char sep = '/';

    if (m_outputDirPath != path) {
        m_outputDirPath = path;

        if (m_outputDirPath.back() != sep) {
            m_outputDirPath.push_back(sep);
        }
    }
}

void plotWrite(const std::string &filename)
{
    m_filename = filename;

    for (std::size_t i = 0; i < m_curves.size(); ++i) {
        writeDatFile(i);
    }

    writeGnuplotFile();

    callGnuplot();
    //        cleanGnuplotFiles();
}

} // namespace edsp
