#include "synthesizers.h"

#include "generators.h"

namespace edsp {

void synthSine(DoubleVector &data, double f, double ampl,
               unsigned int duration, double fs)
{
    double seconds = static_cast<double>(duration) / 1000;

    int count = static_cast<int>(seconds * fs);

    genSine(data, f, ampl, fs, count);
}

void synthSineScale(DoubleVector &data, double fs,
					const std::initializer_list<NotePlay> &values)
{
	for (const NotePlay &np : values) {
		double f = std::get<0>(np);
		double ampl = std::get<1>(np);
		unsigned int duration = std::get<2>(np);

        synthSine(data, f, ampl, duration, fs);
	}
}

} // namespace edsp
