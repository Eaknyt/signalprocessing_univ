#include "wave.h"

#include <algorithm>
#include <fstream>
#include <iostream>
#include <math.h>

#include "utils.h"

namespace edsp {
namespace internal {

class WaveFile
{
public:
    WaveFile();

    WaveFile(const std::uint8_t *data8, // Tableau de donées lorsque l'on est sur des données 8 bits
             long int data_nb,     // Nombre de données
             short channels_nb,    // Nombre de canaux (1 pour mono ou 2 pour stéréo)
             int sampling_freq);   // Fréquence d'échantillonnage (en Hertz)(classique en musique : 44100 Hz)
    WaveFile(const short *data16,      // Tableau de données
             long int data_nb,   // Nombre de données
             short channels_nb,  // Nombre de canaux (1 pour mono ou 2 pour stéréo)
             int sampling_freq); // Fréquence d'échantillonnage (en Hertz)(classique en musique : 44100 Hz)
    ~WaveFile();

    //Méthode permettant de récupérer les données (cad le signal sur 8 bits)
    void getData8(std::uint8_t **data, // pointeur sur le tableau de données lorsque l'on est sur des données 8 bits
                  int *size);           // pointeur sur la taille du tableau

    //Méthode permettant de modifier les données
    void modifData8(std::uint8_t *data); // Tableau de données lorsque l'on est sur des données 8 bits

    void read(const char *fileName);   //Lecture d'un fichier wave
    void write(const char *fileName);

    int samplingFrequency() const;

private :
    //chunk type
    char m_file_type[4];         // (4 octets) : Constante "RIFF" i.e identification du m_format
    int m_file_size;             // (4 octets) : m_file_size est le nombre d'octet restant é lire (i.e = taille du fichier moins 8 octets)
    char m_file_id[4];           // (4 octets) : Identifiant "WAVE"

    //chunk format
    char m_chunk_id[4];          // (4 octets) : Identifiant "fmt "
    int m_chunk_size;            // (4 octets) : Nombre d'octets utilisés pour définir en détail le chunk
    short m_format;              // (2 octets) : Format de fichier (1: PCM,  ...)
    short m_channels_nb;         // (2 octets) : Nombre de canaux (1 pour mono ou 2 pour stéréo)
    int m_sampling_freq;         // (4 octets) : Fréquence d'échantillonnage (en Hertz)
    int m_bytes_per_second;      // (4 octets) : Nombre d'octets par seconde de musique
    short m_bytes_per_sample;    // (2 octets) : Nombre d'octets par échantillon
    short m_depth;               // (2 octets) : Nombre de bits par donnée (8 ou 16)

    //chunk données
    char m_data_id[4];          // (4 octets) : Constante "data"
    int m_data_size;            // (4 octets) : nombre d'octet restant (i.e = taille du fichier moins 44 octets)

    //data
//    Numeric *m_data;
    std::uint8_t *m_data8;     // Tableau de donées lorsque l'on est sur des données 8 bits
    short *m_data16;            // Tableau de donées lorsque l'on est sur des données 16 bits
    long int m_data_nb;         // Nombre de données

    bool m_is_data8_allocated;  // Indique si le tableau é été alloué
    bool m_is_data16_allocated; // Indique si le tableau é été alloué

    void initDescriptor(int depth,          // Nombre de bits par donnée (8 ou 16)
                        short channels_nb,  // Nombre de canaux (1 pour mono ou 2 pour stéréo)
                        int sampling_freq); // Fréquence d'échantillonnage (en Hertz)(classique en musique : 44100 Hz)
};

WaveFile::WaveFile()
{
    //Vérifie que tout est correct pour la taille des types
    m_is_data8_allocated = false; // Tableau non encore alloué
    m_is_data16_allocated = false; // Tableau non encore alloué
}

WaveFile::WaveFile(const std::uint8_t *data8, // Tableau de donées lorsque l'on est sur des données 8 bits
                   long int data_nb,     // Nombre de données
                   short channels_nb,    // Nombre de canaux (1 pour mono ou 2 pour stéréo)
                   int sampling_freq)    // Fréquence d'échantillonnage (en Hertz)(classique en musique : 44100 Hz)
{
    int i;

    // Nombre de données
    m_data_nb = data_nb;

    // Tableau de données lorsque l'on est sur des données 16 bits
    m_data8 = new std::uint8_t[data_nb];

    for (i = 0; i < data_nb; i++) {
        m_data8[i] = data8[i]; //Recopie en profondeur
    }

    m_is_data8_allocated = true;  // Tableau alloué
    m_is_data16_allocated = false; // Tableau non alloué

    initDescriptor(8, channels_nb, sampling_freq);
}

WaveFile::WaveFile(const short *data16,       // Tableau de données
                   long int data_nb,    // Nombre de données
                   short channels_nb,   // Nombre de canaux (1 pour mono ou 2 pour stéréo)
                   int sampling_freq)  // Fréquence d'échantillonnage (en Hertz)(classique en musique : 44100 Hz)
{
    int i;

    // Nombre de données
    m_data_nb = data_nb;

    // Tableau de donées lorsque l'on est sur des données 16 bits
    m_data16 = new short[data_nb];

    for (i = 0; i < data_nb; i++) {
        m_data16[i] = data16[i]; //Recopie en profondeur
    }

    m_is_data8_allocated = false; // Tableau non alloué
    m_is_data16_allocated = true;  // Tableau alloué

    initDescriptor(16, channels_nb, sampling_freq);
}

void
WaveFile::initDescriptor(int depth,           // Nombre de bits par donnée (8 ou 16)
                         short channels_nb,   // Nombre de canaux (1 pour mono ou 2 pour stéréo)
                         int sampling_freq)
{ // Fréquence d'échantillonnage (en Hertz)(classique en musique : 44100 Hz)

    // (2 octets) : Nombre de bits par donnée (8 ou 16)
    m_depth = depth;

    // (4 octets) : Constante "RIFF" i.e identification du m_format
    m_file_type[0] = 'R';
    m_file_type[1] = 'I';
    m_file_type[2] = 'F';
    m_file_type[3] = 'F';
    // (4 octets) : Identifiant "WAVE"
    m_file_id[0] = 'W';
    m_file_id[1] = 'A';
    m_file_id[2] = 'V';
    m_file_id[3] = 'E';
    // (4 octets) : Identifiant "fmt "
    m_chunk_id[0] = 'f';
    m_chunk_id[1] = 'm';
    m_chunk_id[2] = 't';
    m_chunk_id[3] = (std::uint8_t) 32;
    // (4 octets) : Constante "data"
    m_data_id[0] = 'd';
    m_data_id[1] = 'a';
    m_data_id[2] = 't';
    m_data_id[3] = 'a';

    // (4 octets) : m_file_size est le nombre d'octet restant é lire (i.e = taille du fichier moins 8 octets)
    m_file_size = 44 + (depth / 8) * m_data_nb - 8;
    // (4 octets) : Nombre d'octets utilisés pour définir en détail le chunk
    m_chunk_size = 16;
    // (2 octets) : Format de fichier (1: PCM,  ...)
    m_format = 1;
    // (2 octets) : Nombre de canaux (1 pour mono ou 2 pour stéréo)
    m_channels_nb = channels_nb;
    // (4 octets) : Fréquence d'échantillonnage (en Hertz)
    m_sampling_freq = sampling_freq;
    // (4 octets) : Nombre d'octets par seconde de musique
    m_bytes_per_second = sampling_freq * channels_nb * depth / 8;
    // (2 octets) : Nombre d'octets par échantillon
    m_bytes_per_sample = channels_nb * depth / 8;
    // (4 octets) : nombre d'octet restant (i.e = taille du fichier moins 44 octets)
    m_data_size = (depth / 8) * m_data_nb;
}

WaveFile::~WaveFile()
{
    if (m_is_data8_allocated) {
        delete[] m_data8;
    }

    if (m_is_data16_allocated) {
        delete[] m_data16;
    }
}

void
WaveFile::getData8(std::uint8_t **data, // Tableau de donées lorsque l'on est sur des données 8 bits
                   int *size)
{          // Taille du tableau

    int i;

    if (!m_is_data8_allocated) { // Tableau non encore alloué
        std::cout << "WaveFile::getData8: Erreur, les donnée ne sont pas présente en 8 bits \n";
        return;
    }

    // Nombre de données
    (*size) = m_data_nb;

    // Allocation du tableau de données
    (*data) = new std::uint8_t[m_data_nb];

    for (i = 0; i < m_data_nb; i++) {
        (*data)[i] = m_data8[i]; //Recopie en profondeur
    }
}

void WaveFile::modifData8(std::uint8_t *data)
{ // Tableau de donées lorsque l'on est sur des données 8 bits

    int i;

    if (!m_is_data8_allocated) { // Tableau non encore alloué
        std::cout << "WaveFile::setData8: Erreur, le tableau n'est pas alloué\n";
        return;
    }

    // Remplissage du tableau
    for (i = 0; i < m_data_nb; i++) {
        m_data8[i] = data[i];       //Recopie en profondeur
    }
}

void WaveFile::read(const char *fileName)
{

    int pos = 0;
    int i;
    std::uint8_t c;
    std::uint8_t str_tmp[4];
    std::uint8_t header[44];

    FILE *fd = fopen(fileName, "rb");
    // TEST D'OUVERTURE
    if (fd == NULL) {
        std::cout << "WaveFile::read: Erreur, le fichier " << fileName
                  << " n'est pas ouvrable\n";
        fclose(fd);
        return;
    }

    // TEST DE LECTURE
    if (fread(header, 1, 44, fd) != 44) {
        std::cout << "WaveFile::read: Erreur, impossible de lire dans le fichier "
                  << fileName << " les 44 octets d'entete\n";
        fclose(fd);
        return;
    }

    //for (i=0; i<44; i++) {
    //cout<<(int)header[i]<<endl;
    //}

    // m_file_type
    for (i = 0; i < 4; i++, pos++) {
        m_file_type[i] =
            header[pos]; // Constante "RIFF" i.e identification du m_format
        std::cout << m_file_type[i];
    }

    if (!((m_file_type[0] == 'R') && (m_file_type[1] == 'I')
        && (m_file_type[2] == 'F') && (m_file_type[3] == 'F'))) {
        std::cout << "WaveFile::read: Erreur, le fichier " << fileName
                  << " n'est pas un fichier RIFF\n";
        fclose(fd);
        return;
    }

    std::cout << std::endl;

    // m_file_size
    m_file_size = 0;

    for (i = 0; i < 4; i++, pos++) {
        //Bit de poids faible en premier
        m_file_size += (int) pow(256, i)
            * (int) header[pos]; //little-endian : increasing numeric significance with increasing memory addresses
    }

    std::cout << "WaveFile::read: Le nombre d'octets restant é lire est : "
              << m_file_size << std::endl;

    // m_file_id
    for (i = 0; i < 4; i++, pos++) {
        m_file_id[i] = header[pos];         // Identifiant "WAVE"
        std::cout << m_file_id[i];
    }

    std::cout << std::endl;

    if (!((m_file_id[0] == 'W') && (m_file_id[1] == 'A')
        && (m_file_id[2] == 'V') && (m_file_id[3] == 'E'))) {
        std::cout << "WaveFile::read: Erreur, le fichier " << fileName
             << " n'est pas un fichier WAVE\n";
        fclose(fd);
        return;
    }

    // m_chunk_id
    for (i = 0; i < 4; i++, pos++) {
        m_chunk_id[i] = header[pos];         // Identifiant "fmt "
        std::cout << m_chunk_id[i];
    }

    std::cout << std::endl;

    if (!((m_chunk_id[0] == 'f') && (m_chunk_id[1] == 'm')
        && (m_chunk_id[2] == 't') /*&& (m_chunk_id[3] == ' ')*/)) {
        std::cout << "WaveFile::read: Erreur, le fichier " << fileName
             << " n'est pas un fichier 'fmt'\n";
        fclose(fd);
        return;
    }

    // m_chunk_size
    m_chunk_size =
        0; // Nombre d'octets utilisés pour définir en détail le chunk

    for (i = 0; i < 4; i++, pos++) {
        //Bit de poids faible en premier
        m_chunk_size += (int) pow(256, i)
            * (int) header[pos]; //little-endian : increasing numeric significance with increasing memory addresses
    }

    std::cout << "WaveFile::read: m_chunk_size vaut : " << m_chunk_size
              << std::endl;

    // m_format
    m_format = 0;     // Format de fichier (1: PCM,  ...)

    for (i = 0; i < 2; i++, pos++) {
        //Bit de poids faible en premier
        m_format += (int) pow(256, i)
            * (int) header[pos]; //little-endian : increasing numeric significance with increasing memory addresses
    }

    std::cout << "WaveFile::read: Le m_format est (1=PCM) : " << m_format
              << std::endl;

    // m_format
    m_channels_nb = 0;     // Nombre de canaux (1 pour mono ou 2 pour stéréo)

    for (i = 0; i < 2; i++, pos++) {
        //Bit de poids faible en premier
        m_channels_nb += (int) pow(256, i)
            * (int) header[pos]; //little-endian : increasing numeric significance with increasing memory addresses
    }

    std::cout << "WaveFile::read: Le nombre de canaux est : " << m_channels_nb
              << std::endl;

    // m_sampling_freq
    m_sampling_freq = 0;     // Fréquence d'échantillonnage (en Hertz)

    for (i = 0; i < 4; i++, pos++) {
        //Bit de poids faible en premier
        m_sampling_freq += (int) pow(256, i)
            * (int) header[pos]; //little-endian : increasing numeric significance with increasing memory addresses
    }

    std::cout << "WaveFile::read: La fréquence d'échantillonge est : "
              << m_sampling_freq << std::endl;

    // m_bytes_per_second
    m_bytes_per_second = 0;     // Nombre d'octets par seconde de musique

    for (i = 0; i < 4; i++, pos++) {
        //Bit de poids faible en premier
        m_bytes_per_second += (int) pow(256, i)
            * (int) header[pos]; //little-endian : increasing numeric significance with increasing memory addresses
    }

    std::cout << "WaveFile::read: Le nombre d'octets par seconde est : "
              << m_bytes_per_second << std::endl;

    // m_bytes_per_sample
    m_bytes_per_sample = 0;     // Nombre d'octets par échantillon

    for (i = 0; i < 2; i++, pos++) {
        //Bit de poids faible en premier
        m_bytes_per_sample += (int) pow(256, i)
            * (int) header[pos]; //little-endian : increasing numeric significance with increasing memory addresses
    }

    std::cout << "WaveFile::read: Le nombre d'octets par échantillon est : "
              << m_bytes_per_sample << std::endl;

    if (m_bytes_per_sample != (m_bytes_per_second / m_sampling_freq)) {
        std::cout << "WaveFile::read: m_bytes_per_sample != (m_bytes_per_second/m_sampling_freq)\n";
        fclose(fd);
        return;
    }

    // m_depth
    m_depth = 0;     // Nombre de bits par donnée (8 ou 16)

    for (i = 0; i < 2; i++, pos++) {
        //Bit de poids faible en premier
        m_depth += (int) std::pow(256, i)
            * (int) header[pos]; //little-endian : increasing numeric significance with increasing memory addresses
    }

    std::cout << "WaveFile::read: Le nombre de bits par donnée est : "
              << m_depth << std::endl;

    if (m_depth != (8 * m_bytes_per_sample / m_channels_nb)) {
        std::cout << "WaveFile::read: m_depth != (8*m_bytes_per_sample/m_channels_nb)\n";
        fclose(fd);
        return;
    }

    //chunk données
    for (i = 0; i < 4; i++, pos++) {
        m_data_id[i] = header[pos]; // Constante "data"
        std::cout << m_data_id[i];
    }

    std::cout << std::endl;

    if (!((m_data_id[0] == 'd') && (m_data_id[1] == 'a')
        && (m_data_id[2] == 't') && (m_data_id[3] == 'a'))) {
        std::cout << "WaveFile::read: Erreur, le fichier " << fileName
                  << " est corrompu (pas de chaine 'data' dans le header)\n";
        fclose(fd);
        return;
    }

    // m_data_size
    m_data_size =
        0;     // nombre d'octet restant (i.e = taille du fichier moins 44 octets)

    for (i = 0; i < 4; i++, pos++) {
        //Bit de poids faible en premier
        m_data_size += (int) pow(256, i)
            * (int) header[pos]; //little-endian : increasing numeric significance with increasing memory addresses
    }

    std::cout << "WaveFile::read: Le nombre d'octets restant é lire est : "
              << m_data_size << std::endl;

    if (m_data_size != (m_file_size - 36)) {
        std::cout << "WaveFile::read: m_data_size != (m_file_size-36)\n";
        fclose(fd);
        return;
    }

    //LECTURE DES DONNEES
    switch (m_depth) {
    case 8:
        m_data_nb = m_data_size;
        m_data8 = new std::uint8_t[m_data_nb];
        if (fread(m_data8, 1, m_data_nb, fd)
            != (size_t) m_data_nb) { //Les donnees sont sur 8 bits
            std::cout << "WaveFile::read: Erreur, impossible de lire dans le fichier "
                      << fileName << " le bon nombre d'octet\n";
            delete [] m_data8;
            fclose(fd);
            return;
        }
        m_is_data8_allocated = true;  // Tableau non alloué
        m_is_data16_allocated = false; // Tableau alloué
        break;

    case 16:
        m_data_nb = m_data_size / 2;
        m_data16 = new short[m_data_nb];
        if (fread(m_data16, 2, m_data_nb, fd)
            != (size_t) m_data_nb) { //Les donnees sont sur 16 bits
            std::cout << "WaveFile::read: Erreur, impossible de lire dans le fichier "
                      << fileName << " le bon nombre d'octet\n";
            delete [] m_data8;
            fclose(fd);
            return;
        }
        m_is_data8_allocated = false; // Tableau non alloué
        m_is_data16_allocated = true;  // Tableau alloué
        break;

    default:
        std::cout << "WaveFile::read: Erreur, la profondeur (m_depth = " << m_depth
                  << ") est inconnue\n";
        fclose(fd);
        return;
    }

    //FERMETURE DU FICHIER
    fclose(fd);
}

void WaveFile::write(const char *fileName)
{
    std::cout << "WaveFile::write(char* fileName)\n";

    int pos = 0;
    int i;
    std::uint8_t header[44];

    FILE *fd = fopen(fileName, "wb");

    // TEST D'OUVERTURE
    if (fd == NULL) {
        std::cout << "WaveFile::write: Erreur, le fichier " << fileName
                  << " n'est pas ouvrable\n";
        fclose(fd);
        return;
    }

    // m_file_type : constante "RIFF" i.e identification du m_format
    for (i = 0; i < 4; i++, pos++) {
        header[pos] =  m_file_type[i]; // Constante "RIFF" i.e identification du m_format
    }

    // m_file_size
    std::cout << "m_file_size = " << m_file_size << std::endl;

    for (i = 0; i < 4; i++, pos++) {
        //Bit de poids faible en premier
        header[pos] = ((255 << (8 * i)) & m_file_size) >> (8 * i); //little-endian : increasing numeric significance with increasing memory addresses
//    cout<<"header[pos] = "<<(int)header[pos];
    }

    // m_file_id : identifiant "WAVE"
    for (i = 0; i < 4; i++, pos++) {
        header[pos] = m_file_id[i];
    }

    // m_chunk_id : identifiant "fmt "
    for (i = 0; i < 4; i++, pos++) {
        header[pos] = m_chunk_id[i];
    }

    if (!((header[pos - 4] == 'f') && (header[pos - 3] == 'm')
        && (header[pos - 2] == 't') /*&& (m_chunk_id[3] == ' ')*/)) {
        std::cout << header[pos - 4] << header[pos - 3] << header[pos - 2]
                  << header[pos - 1] << std::endl;
        std::cout
            << "WaveFile::write: Erreur, le header est mal remplit pour le m_chunk_id 'fmt'\n";
        fclose(fd);
        return;
    }

    // m_chunk_size : nombre d'octets utilisés pour définir en détail le chunk
    for (i = 0; i < 4; i++, pos++) {
        //Bit de poids faible en premier
        header[pos] = ((255 << (8 * i)) & m_chunk_size) >> (8 * i); //little-endian : increasing numeric significance with increasing memory addresses
    }

    // m_format : m_format de fichier (1: PCM,  ...)
    for (i = 0; i < 2; i++, pos++) {
        //Bit de poids faible en premier
        header[pos] = ((255 << (8 * i)) & m_format) >> (8 * i); //little-endian : increasing numeric significance with increasing memory addresses
    }

    // m_channels_nb : Nombre de canaux (1 pour mono ou 2 pour stéréo)
    for (i = 0; i < 2; i++, pos++) {
        //Bit de poids faible en premier
        header[pos] = ((255 << (8 * i)) & m_channels_nb) >> (8
            * i); //little-endian : increasing numeric significance with increasing memory addresses
    }

    // m_sampling_freq : fréquence d'échantillonge
    for (i = 0; i < 4; i++, pos++) {
        //Bit de poids faible en premier
        header[pos] = ((255 << (8 * i)) & m_sampling_freq) >> (8 * i); //little-endian : increasing numeric significance with increasing memory addresses
    }

    // m_bytes_per_second : nombre d'octets par seconde de musique
    for (i = 0; i < 4; i++, pos++) {
        //Bit de poids faible en premier
        header[pos] = ((255 << (8 * i)) & m_bytes_per_second) >> (8 * i); //little-endian : increasing numeric significance with increasing memory addresses
    }

    // m_bytes_per_sample : nombre d'octets par échantillon
    for (i = 0; i < 2; i++, pos++) {
        //Bit de poids faible en premier
        header[pos] = ((255 << (8 * i)) & m_bytes_per_sample) >> (8 * i); //little-endian : increasing numeric significance with increasing memory addresses
    }

    // m_depth : nombre de bits par donnée (8 ou 16)
    for (i = 0; i < 2; i++, pos++) {
        //Bit de poids faible en premier
        header[pos] = ((255 << (8 * i)) & m_depth) >> (8 * i); //little-endian : increasing numeric significance with increasing memory addresses
    }

    //chunk données : constante "data"
    for (i = 0; i < 4; i++, pos++) {
        header[pos] = m_data_id[i]; // Constante "data"
    }

    // m_data_size : nombre d'octet restant (i.e = taille du fichier moins 44 octets)
    for (i = 0; i < 4; i++, pos++) {
        //Bit de poids faible en premier
        header[pos] = ((255 << (8 * i)) & m_data_size) >> (8 * i); //little-endian : increasing numeric significance with increasing memory addresses
    }

    // ECRITURE DU HEADER
    if (fwrite(header, 1, 44, fd) != 44) {
        std::cout
            << "WaveFile::write: Erreur, impossible d'écrire dans le fichier "
            << fileName << " le header\n";
        fclose(fd);
        return;
    }

    std::cout << "Fin écriture du header\n";

    //LECTURE DES DONNEES
    switch (m_depth) {
    case 8:
        std::cout << "fwrite (m_data8, \n";
        if (fwrite(m_data8, 1, m_data_nb, fd)
            != (size_t) m_data_nb) { //Les donnees sont sur 8 bits
            std::cout
                << "WaveFile::write: Erreur, impossible d'écrire dans le fichier "
                << fileName << " le bon nombre d'octet\n";
            fclose(fd);
            return;
        }
        break;
    case 16:
        if (fwrite(m_data16, 2, m_data_nb, fd)
            != (size_t) m_data_nb) { //Les donnees sont sur 16 bits
            std::cout
                << "WaveFile::write: Erreur, impossible d'écrire dans le fichier "
                << fileName << " le bon nombre d'octet\n";
            fclose(fd);
            return;
        }
        break;
    default:
        std::cout << "WaveFile::write: Erreur, la profondeur (m_depth = "
                  << m_depth << ") est inconnue\n";
        fclose(fd);
        return;
    }

    std::cout << "Fin écriture des datas\n";

    //FERMETURE DU FICHIER
    fclose(fd);
}

int WaveFile::samplingFrequency() const
{
    return m_sampling_freq;
}

} // namespace internal


////////////////////////// Implementations //////////////////////////

void wavNormalize(DoubleVector &data)
{
    double xMin = *std::min_element(data.begin(), data.end());
    double xMax = *std::max_element(data.begin(), data.end());

    double a = 255 / (xMax - xMin);
    double b = (255 * xMin) / (xMax - xMin);

    for (std::size_t i = 0; i < data.size(); ++i) {
        data[i] = std::floor(a * data[i] + b);
    }
}

DoubleVector wavRead(const std::string &fileName, double *fs)
{
    internal::WaveFile file;
    file.read(fileName.c_str());

    int size = -1;

    std::uint8_t *tempData = nullptr;

    file.getData8(&tempData, &size);

    if (size == -1) {
        std::cerr << "wavRead() failed" << std::endl;
        return DoubleVector();
    }

    DoubleVector ret(size);

    for (std::size_t i = 0; i < size; ++i) {
        ret[i] = tempData[i];
    }

    delete []tempData;

    if (fs) {
        *fs = file.samplingFrequency();
    }

    return ret;
}

void wavWrite(const DoubleVector &data, const std::string &fileName, double fs)
{
    double xMax = *std::max_element(data.begin(), data.end());

    DoubleVector copy(data.begin(), data.end());

    if (xMax > 255.) {
        wavNormalize(copy);
    }

    std::vector<unsigned char> dataChar(copy.begin(), copy.end());

    internal::WaveFile file(dataChar.data(), dataChar.size(), 1, fs);
    file.write(fileName.c_str());
}

} // namespace edsp
