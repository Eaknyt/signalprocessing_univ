#ifndef WAVEFILE_H
#define WAVEFILE_H

#include "typealiases.h"

namespace edsp {

void wavNormalize(DoubleVector &data);

DoubleVector wavRead(const std::string &fileName, double *fs = nullptr);
void wavWrite(const DoubleVector &data, const std::string &fileName,
              double fs = 44100);

} // namespace edsp

#endif // WAVEFILE_H
