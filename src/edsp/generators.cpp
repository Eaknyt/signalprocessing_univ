#include "generators.h"

#include "edsp_constants.h"


////////////////////////// Helpers //////////////////////////

namespace {

void resizeVector(edsp::DoubleVector &data, int supCount)
{
    std::size_t initialSize = data.size();
    
    std::size_t newSize = initialSize + supCount;

    if (initialSize < newSize) {
        data.resize(newSize);
    }
}

} // anon namespace

namespace edsp {

void genSquare(DoubleVector &data, double f, double ampl,
               double fs, int count)
{
    std::size_t N = data.size();

    resizeVector(data, count);

    for (int k = N; k < N + count; ++k) {
        double value = (static_cast<int>(k / f) % 2) ? ampl : -ampl;

        data[k] = value;
    }
}

void genSine(DoubleVector &data, double f, double ampl, double fs, int count)
{
    std::size_t N = data.size();

    resizeVector(data, count);

    double K = (1. / f) / (1. / fs);

    for (int k = N; k < N + count; ++k) {
        double value = (ampl * std::sin((M_PI_x2 * static_cast<double>(k)) / K)) + ampl;

        data[k] = value;
    }
}

} // namespace edsp
