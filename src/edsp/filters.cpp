#include "filters.h"

#include "fourier.h"
#include "utils.h"

namespace {

} // anon namespace

////////////////////////// Implementations //////////////////////////

namespace edsp {

DoubleVector filter(const DoubleVector &data, const FilterCoeffs &coeffs)
{
    DoubleVector ret(data.begin(), data.end());

    for (std::size_t i = 0; i < data.size(); ++i) {
    }

    return ret;
}

void cutFreqBelow(ComplexVector &spectrum, double fs, double fc)
{
    std::size_t N = spectrum.size();

    for (std::size_t i = 0; i < N / 2; ++i) {
        double freq = static_cast<double>(i) * (fs / N);

        if (0 < freq && freq < fc) {
            spectrum[i] = 0;
            spectrum[N - i] = 0;
        }
    }
}

void cutFreqAbove(ComplexVector &spectrum, double fs, double fc)
{
    std::size_t N = spectrum.size();

    for (std::size_t i = 0; i < N / 2; ++i) {
        double freq = static_cast<double>(i) * (fs / N);

        if (freq > fc) {
            spectrum[i] = 0;
            spectrum[N - i] = 0;
        }
    }
}

} // namespace edsp
