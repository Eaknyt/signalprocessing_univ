#ifndef EDSP_UTILS_H
#define EDSP_UTILS_H

#include <algorithm>
#include <cstddef>

#include "typealiases.h"

namespace edsp {

template <class ForwardIterator, typename T>
void iota_step(ForwardIterator first, ForwardIterator last, T value, T step)
{
    while (first != last) {
        *first++ = value;

        value += step;
    }
}

template <class ForwardIterator, typename T>
ForwardIterator find_or_nearest(ForwardIterator first, ForwardIterator last,
                                const T &value)
{
    auto nearest = std::find_if(first, last, [=] (const T &eltVal) {
        return (eltVal >= value);
    });

    return nearest;
}

bool isPowerOfTwo(std::size_t x);

std::size_t nextPowerOfTwo(std::size_t x);

DoubleVector real(const ComplexVector &Z);
DoubleVector imag(const ComplexVector &Z);

DoubleVector linSpace(double from, double to, int N);

DoubleVector normalize(const DoubleVector &data);

DoubleVector normalize(const DoubleVector &data, double min, double max);
DoubleVector denormalize(const DoubleVector &data, double min, double max);

} // namespace edsp

#endif // EDSP_UTILS_H
