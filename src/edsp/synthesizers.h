#ifndef EDSP_SYNTHESIZER_H
#define EDSP_SYNTHESIZER_H

#include <initializer_list>
#include <tuple>

#include "typealiases.h"

namespace edsp {

void synthSine(DoubleVector &data, double f, double ampl,
               unsigned int duration, double fs);

using NotePlay = std::tuple<double, double, unsigned int>;

void synthSineScale(DoubleVector &data, double fs,
					const std::initializer_list<NotePlay> &values);

} // namespace edsp

#endif // EDSP_SYNSHETIZER_H
