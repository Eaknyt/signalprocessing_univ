#ifndef FOURIER_HPP
#define FOURIER_HPP

#include "typealiases.h"
#include "utils.h"

namespace edsp {

template <typename T>
void zeros(std::vector<T> &x)
{
    std::size_t N = x.size();

    if (!isPowerOfTwo(N)) {
        std::size_t newSize = nextPowerOfTwo(N);

        if (newSize > N) {
            x.resize(newSize);
        }
    }
}

ComplexVector fft(const DoubleVector &x);
ComplexVector ifft(const ComplexVector &X);

DoubleVector abs(const ComplexVector &cSig);

template <class Container>
Container fftShift(const Container &data)
{
    Container ret(data.begin(), data.end());

    std::size_t N = data.size();
    auto centerIt = ret.begin() + (N / 2);

    std::rotate(ret.begin(), centerIt, ret.end());

    return ret;
}

DoubleVector fftFreq(const DoubleVector &points, double fs);

} // namespace edsp

#endif // FOURIER_HPP
