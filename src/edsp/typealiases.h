#ifndef EDSP_TYPEALIASES_H
#define EDSP_TYPEALIASES_H

#include <complex>
#include <cstdint>
#include <tuple>
#include <vector>

namespace edsp {

using Frequency = double;
using Complex = std::complex<double>;

using DoubleVector = std::vector<double>;

using ComplexVector = std::vector<Complex>;

using FilterCoeffs = std::tuple<DoubleVector, DoubleVector>;

} // namespace edsp

#endif // EDSP_TYPEALIASES_H
