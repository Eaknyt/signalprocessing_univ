#ifndef EDSP_FILTERS_H
#define EDSP_FILTERS_H

#include "typealiases.h"

namespace edsp {

DoubleVector filter(const DoubleVector &data, const FilterCoeffs &coeffs);

void cutFreqBelow(ComplexVector &spectrum, double fs, double fc);
void cutFreqAbove(ComplexVector &spectrum, double fs, double fc);

} // namespace edsp

#endif // EDSP_FILTERS_H
