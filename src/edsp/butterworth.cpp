#include "butterworth.h"

namespace {

} // anon namespace


////////////////////////// Implementations //////////////////////////

namespace edsp {

double butterAlpha(double f, double fs)
{
    return M_PI * (f / fs);
}

FilterCoeffs butter(double f, double fs)
{
    const double alpha = butterAlpha(f, fs);

    const double alpha_x2 = 2 * alpha;
    const double alpha_p2 = std::pow(alpha, 2);
    const double alpha_p3 = std::pow(alpha, 3);

    const double A = 1 + alpha_x2 + 2 * alpha_p2 + alpha_p3;
    const double B = -3 - alpha_x2 + 2 * alpha_p2 + 3 * alpha_p3;
    const double C = 3 - alpha_x2 - 2 * alpha_p2 + 3 * alpha_p3;
    const double D = -1 + alpha_x2 - 2 * alpha_p2 + alpha_p3;

    // Coefficients
    double inputC0 = alpha_p3 / A;
    double inputC1 = (3 * alpha_p3) / A;

    double outputC0 = B / A;
    double outputC1 = C / A;
    double outputC2 = D / A;

    DoubleVector a {inputC0, inputC1, inputC1, inputC0};
    DoubleVector b {-outputC0, -outputC1, -outputC2};

    return FilterCoeffs(b, a);
}

DoubleVector butter(const DoubleVector &data, double f, double fs)
{
    DoubleVector ret(data.begin(), data.end());

    FilterCoeffs coeffs = butter(f, fs);
    DoubleVector outputCoeffs = std::get<0>(coeffs);
    DoubleVector inputCoeffs = std::get<1>(coeffs);

    for (std::size_t i = 3; i < ret.size(); ++i) {
        ret[i] =  inputCoeffs[0] * data[i]
                + inputCoeffs[1] * data[i - 1]
                + inputCoeffs[2] * data[i - 2]
                + inputCoeffs[3] * data[i - 3]
                + outputCoeffs[0] * ret[i - 1]
                + outputCoeffs[1] * ret[i - 2]
                + outputCoeffs[2] * ret[i - 3];
    }

    return ret;
}

} // namespace edsp
