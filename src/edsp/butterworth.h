#ifndef EDSP_BUTTERWORTH_H
#define EDSP_BUTTERWORTH_H

#include "typealiases.h"

namespace edsp {

double butterAlpha(double f, double fs);

FilterCoeffs butter(double f, double fs);
DoubleVector butter(const DoubleVector &data, double f, double fs);

} // namespace edsp

#endif // EDSP_BUTTERWORTH_H
