#ifndef EDSP_GENERATORS_H
#define EDSP_GENERATORS_H

#include <cstddef>

#include "typealiases.h"

namespace edsp {

void genSquare(DoubleVector &data, double f, double ampl, double fs,
               int count);
void genSine(DoubleVector &data, double f, double ampl, double fs,
             int count);

} // namespace edsp

#endif //EDSP_GENERATORS_H
