#include "utils.h"

#include <array>
#include <numeric>

namespace {

constexpr const std::array<std::size_t, 32> powersOfTwo = {
    1, 2, 4, 8, 16, 32, 64, 128, 256, 512, 1024, 2048, 4096, 8192, 16384,
    32768, 65536, 131072, 262144, 524288, 1048576, 2097152, 4194304, 8388608,
    16777216, 33554432, 67108864, 134217728, 268435456, 536870912, 1073741824,
    2147483648
};

} // anon namespace

namespace edsp {

bool isPowerOfTwo(std::size_t x)
{
    auto it = std::find(powersOfTwo.begin(), powersOfTwo.end(), x);

    return (it != powersOfTwo.end());
}

std::size_t nextPowerOfTwo(std::size_t x)
{
    auto it = std::find_if(powersOfTwo.begin(), powersOfTwo.end(),
                           [x](std::size_t value) {
            return x < value;
});

    if (it != powersOfTwo.end()) {
        return (it != powersOfTwo.begin()) ? *(it--) : (*it);
    }

    return 0;
}

DoubleVector real(const ComplexVector &Z)
{
    std::size_t N = Z.size();

    DoubleVector ret(N);

    for (int i = 0; i < N; ++i) {
        const Complex &z = Z[i];

        ret[i] = z.real();
    }

    return ret;
}

DoubleVector imag(const ComplexVector &Z)
{
    std::size_t N = Z.size();

    DoubleVector ret(N);

    for (int i = 0; i < N; ++i) {
        const Complex &z = Z[i];

        ret[i] = z.imag();
    }

    return ret;
}

DoubleVector linSpace(double from, double to, int count)
{
    DoubleVector ret(count);

    if (to - from == count) {
        std::iota(ret.begin(), ret.end(), from);
    }
    else {
        double dx = (to - from) / (count - 1);

        for (std::size_t i = 0; i < count; ++i) {
            ret[i] = from + dx * i;
        }
    }

    return ret;
}

DoubleVector normalize(const DoubleVector &data)
{
    DoubleVector ret = data;

    double max = *std::max_element(ret.begin(), ret.end());

    for (double &v : ret) {
        v /= max;
    }

    return ret;
}

DoubleVector normalize(const DoubleVector &data, double min, double max)
{
    auto oldMinMax = std::minmax_element(data.begin(), data.end());
    double oldMin = (*oldMinMax.first);
    double oldMax = (*oldMinMax.second);

    DoubleVector ret = data;

    std::transform(ret.begin(), ret.end(), ret.begin(), [=] (double value) {
        return (max - min) / (oldMax - oldMin) * (value - oldMax) + max;
    });

    return ret;
}

DoubleVector denormalize(const DoubleVector &data, double min, double max)
{
    auto oldMinMax = std::minmax_element(data.begin(), data.end());
    double oldMin = (*oldMinMax.first);
    double oldMax = (*oldMinMax.second);

    DoubleVector ret = data;

    std::transform(ret.begin(), ret.end(), ret.begin(), [=] (double value) {
        return (value - oldMax) * ((max - min) / (oldMax - oldMin)) + max;
    });

    return ret;
}

} // namespace edsp
