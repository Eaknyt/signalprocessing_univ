#include "fourier.h"

#include <array>
#include <assert.h>
#include <iostream>
#include <math.h>

#include "edsp_constants.h"


namespace {

using namespace edsp;
constexpr const Complex J(0, 1);

// https://en.wikipedia.org/wiki/Cooley%E2%80%93Tukey_FFT_algorithm#Pseudocode
ComplexVector fft_impl(const ComplexVector &x)
{
    std::size_t N = x.size();

    ComplexVector ret;

    // Check the array's size
    if (!isPowerOfTwo(N)) {
        std::cerr << "fft() > x's size must be a power of two" << std::endl;
        return ret;
    }

    ret.resize(N);

    Complex theJ = -J;

    if (N == 1) {
        ret[0] = x[0];
    }
    else {
        std::size_t N_2 = N / 2;

        // Retrieve even and odd elements
        ComplexVector evenElts;
        evenElts.reserve(N_2);
        ComplexVector oddElts;
        oddElts.reserve(N_2);

        for (int n = 0; n < N; ++n) {
            Complex xn = x[n];

            ComplexVector *which = &oddElts;

            if (n % 2 == 0) {
                which = &evenElts;
            }

            which->push_back(xn);
        }

        // /!\ Recursion
        ComplexVector left = fft_impl(evenElts);
        ComplexVector right = fft_impl(oddElts);

        // Merge FFT results
        Complex wv = M_PI_x2 / static_cast<double>(N) * theJ;

        for (std::size_t k = 0; k < N_2; ++k) {
            Complex wk = std::exp(wv * static_cast<double>(k)) * right[k];

            ret[k] = left[k] + wk;
            ret[k + N_2] = left[k] - wk;
        }
    }

    return ret;
}

} // anon namespace


namespace edsp {

ComplexVector fft(const DoubleVector &x)
{
    ComplexVector ret(x.begin(), x.end());
    ret = fft_impl(ret);

    for (Complex &c : ret) {
        c /= ret.size();
    }

    return ret;
}

// https://www.dsprelated.com/showarticle/800.php
ComplexVector ifft(const ComplexVector &X)
{
    ComplexVector ret = X;
    std::reverse(ret.begin() + 1, ret.end());

    ret = fft_impl(ret);

    return ret;
}

DoubleVector abs(const ComplexVector &cSig)
{
    std::size_t N = cSig.size();

    DoubleVector ret;
    ret.reserve(N);

    for (const Complex &z : cSig) {
        double absZ = std::abs(z);

        ret.push_back(absZ);
    }

    return ret;
}

DoubleVector fftFreq(const DoubleVector &points, double fs)
{
    std::size_t N = points.size();

    DoubleVector ret = points;

    for (double &n : ret) {
        n = (n / N - 0.5) * fs;
    }

    return ret;
}

} // namespace edsp
