#ifndef EDSP_CONSTANTS_H
#define EDSP_CONSTANTS_H

#include <math.h>

namespace edsp {

const int SAMPL_FREQ_44100 = 44100;

constexpr const double M_PI_x2 = 2. * M_PI;

} // namespace edsp

#endif // EDSP_CONSTANTS_H
