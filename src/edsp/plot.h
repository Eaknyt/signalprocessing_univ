#ifndef EDSP_PLOTTER_H
#define EDSP_PLOTTER_H

#include <string>

#include "typealiases.h"

namespace edsp {

void plot(const DoubleVector &points, const DoubleVector &values);
void plotAgain(const DoubleVector &points, const DoubleVector &values);
void plotLegend(const std::string &legend, int i = 0);
void plotTitle(const std::string &title);
void plotLabels(const std::string &xLabel, const std::string &yLabel);
void plotArea(double xFrom, double xTo, double yFrom, double yTo);
void plotHighlight(double from, double to, const std::string &color);
void plotResetHighlights();
void plotDirectory(const std::string &path);
void plotWrite(const std::string &filename);

} // namespace edsp

#endif // EDSP_PLOTTER_H
