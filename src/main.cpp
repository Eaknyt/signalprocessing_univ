#include <algorithm>
#include <iostream>
#include <numeric>

#include "music/notes.h"

#include "edsp/butterworth.h"
#include "edsp/edsp_constants.h"
#include "edsp/filters.h"
#include "edsp/fourier.h"
#include "edsp/generators.h"
#include "edsp/plot.h"
#include "edsp/synthesizers.h"
#include "edsp/utils.h"
#include "edsp/wave.h"

using namespace edsp;
using namespace music;

void genLa()
{
    const double ampl = 256;
    const double fs = SAMPL_FREQ_44100;
    const unsigned int duration = 6000;

    // Generate the signal
    std::vector<double> sig;
    synthSine(sig, music::LA, ampl, duration, fs);

    // Create the time vector
    std::size_t N = sig.size();

    std::vector<double> points = linSpace(0, N, N);

    // Plot
    plotTitle("La during 6 seconds");
    plotLabels("Samples", "Amplitude");
    plot(points, sig);
    plotArea(0, 0, 0, 0);
    plotWrite("la");

    //  Zoom-in plot
    plotTitle("La during 6 seconds (zoomed)");
    plotArea(0, 500, 0, 0);
    plotWrite("la_zoomed");

    // Export to .wav
    wavWrite(sig, "output/la.wav");
}

void genLaSpectrum()
{
    const double ampl = 256;
    const double fs = SAMPL_FREQ_44100;
    const unsigned int duration = 6000;

    // Generate the signal
    DoubleVector data;
    synthSine(data, music::LA, ampl, duration, fs);

    // Zero-padding
    zeros(data);

    // Create the time vector
    std::size_t N = data.size();

    DoubleVector points = linSpace(0, N, N);

    // Plot
    plotTitle("La during 6 seconds");
    plotLabels("Samples", "Amplitude");
    plot(points, data);
    plotArea(0, 0, 0, 0);
    plotWrite("la");

    // Compute the FFT
    ComplexVector spectrum = fft(data);

    DoubleVector absSpectrum = abs(spectrum);
    DoubleVector centeredFft = normalize(fftShift(absSpectrum));

    DoubleVector fftPoints = fftFreq(points, fs);

    // Plot spectrum
    plotTitle("La during 6 seconds - Spectrum");
    plotLabels("Frequency (Hz)", "Percentage");
    plot(fftPoints, centeredFft);
    plotArea(-600, 600, 0, 0);
    plotWrite("la_fft_absolute_freq");

    // Compute the IFFT
    DoubleVector ifftValues = real(ifft(spectrum));

    // Plot in time domain again
    plotTitle("La during 6 seconds - Spectrum IFFT");
    plotLabels("Samples", "Amplitude");
    plot(points, ifftValues);
    plotArea(0, 0, 0, 0);
    plotWrite("la_ifft");
} // genLaSpectrum

void genLaScaleWithFourier()
{
    double fs = SAMPL_FREQ_44100;
    const double ampl = 256;
    std::size_t samplesPerNote = 32768;

    DoubleVector sig;

    // Retrieve a sorted vector of the frequencies from the La chromatic scale
    DoubleVector scale = chromaticScale(music::LA);

    for (double note : scale) {
        // Generate the spectrum
        ComplexVector spectrum(samplesPerNote);

        //  Zero bin frequency
        spectrum[0] =  Complex(1);

        //  Generate normalized peaks
        std::size_t i = note / (fs / samplesPerNote);
        spectrum[i] = spectrum[samplesPerNote - i] = Complex(0.5);

        //  Plot the spectrum
        //   Create a human-readable string representation of the note freq
        std::string noteFreqString = std::to_string(note);
        {
            // Remove the extra zeros
            auto lastIt = noteFreqString.end() - 1;
            auto it = lastIt;

            while (*it == '0') {
                it--;
            }

            noteFreqString.erase(it + 1, lastIt);
        }

        DoubleVector fftPoints = linSpace(0, samplesPerNote, samplesPerNote);
        fftPoints = fftFreq(fftPoints, fs);

        plotTitle("Spectrum of a " + noteFreqString + " Hz note");
        plotLabels("Frequency (Hz)", "Percentage");
        plot(fftPoints, normalize(abs(fftShift(spectrum))));
        plotArea(-600, 600, 0, 0);
        plotWrite("spectrum_" + noteFreqString);

        // Reconstruct the note
        DoubleVector noteSig = real(ifft(spectrum));

        //  Recover the right amplitude
        noteSig = denormalize(noteSig, 0, ampl);

        // Append the generated signal to the main one
        sig.insert(sig.end(), noteSig.begin(), noteSig.end());
    }

    // Plot
    std::size_t N = sig.size();
    DoubleVector points = linSpace(0, N, N);

    plotTitle("La chromatic scale using generated from spectrums and ifft");
    plotLabels("Samples", "Amplitude");
    plot(points, sig);
    plotArea(0, 0, 0, 0);
    plotWrite("chroma_scale_from_spectrums");

    // Export to .wav
    wavWrite(sig, "output/chroma_scale_from_spectrums.wav");
} // genLaScaleWithFourier

void plotWaveFile()
{
    // Retrieve and plot the sound
    double fs = 0;
    DoubleVector sound = wavRead("sounds/BrokenGlass.wav", &fs);

    std::size_t N = sound.size();
    DoubleVector points = linSpace(0, N, N);

    plotTitle("BrokenGlass Signal");
    plotLabels("Samples", "Amplitude");
    plot(points, sound);
    plotArea(0, 0, 0, 0);
    plotWrite("broken_glass");

    // Plot the padded signal
    zeros(sound);

    std::size_t cN = sound.size();

    DoubleVector fftPoints = linSpace(0, cN, cN);

    plotTitle("BrokenGlass Signal - Padded");
    plotLabels("Samples", "Amplitude");
    plot(fftPoints, sound);
    plotWrite("broken_glass_padded");

    // fft
    ComplexVector spectrum = fft(sound);

    plotTitle("BrokenGlass Spectrum");
    plotLabels("Frequency (Hz)", "Percentage");
    plot(fftFreq(fftPoints, 22050), normalize(abs(fftShift(spectrum))));
    plotArea(-600, 600, 0, 0.005);
    plotWrite("broken_glass_spectrum");

    // ifft
    ComplexVector icSound = ifft(spectrum);
    DoubleVector ifftSound = real(icSound);

    plotTitle("BrokenGlass Spectrum IFFT");
    plotLabels("Samples", "Amplitude");
    plot(fftPoints, ifftSound);
    plotArea(0, 0, 0, 0);
    plotWrite("broken_glass_ifft_spectrum");

    // Export the ifft sound to .wav
    wavWrite(ifftSound, "output/BrokenGlass_ifft.wav", fs);
}

void filterUsingDft()
{
    double fs = SAMPL_FREQ_44100;
    const double ampl = 256;

    // Create a signal and its spectrum
    DoubleVector data;
    genSine(data, music::DO, ampl, fs, 131072);
    genSine(data, music::SI, ampl, fs, 65536);
    genSine(data, music::DO, ampl, fs, 65536);
    zeros(data); // FIXME weird sound with a padded signal

    wavWrite(data, "output/dosido.wav");

    std::size_t N = data.size();

    DoubleVector points(N);
    std::iota(points.begin(), points.end(), 0);

    plotTitle("DoSiDo Signal");
    plotLabels("Samples", "Amplitude");
    plot(points, data);
    plotWrite("dosido");

    ComplexVector spectrum = fft(data);
    DoubleVector normSpectrum = normalize(abs(fftShift(spectrum)));

    DoubleVector fftPoints = fftFreq(points, fs);

    plotTitle("DoSiDo Spectrum");
    plotLabels("Frequency (Hz)", "Percentage");
    plot(fftPoints, normSpectrum);
    plotArea(-600, 600, 0, 0);
    plotWrite("dosido_spectrum");

    // Filter
    cutFreqBelow(spectrum, fs, music::SI - 50);
    normSpectrum = normalize(abs(fftShift(spectrum)));

    plotTitle("Spectrum of a DoSiDo with frequencies < Si filtered");
    plotLabels("Frequency (Hz)", "Percentage");
    plot(fftPoints, normSpectrum);
//    plotArea(0, 0, 0, 0);
    plotWrite("dosido_si_filtered_spectrum");

    // Ifft the filtered spectrum
    DoubleVector reconstructedDoSiDo = abs(ifft(spectrum));

    plotTitle("DoSiDo Signalwith frequencies < Si filtered");
    plotLabels("Samples", "Amplitude");
    plot(points, reconstructedDoSiDo);
    plotArea(0, 0, 0, 0);
    plotWrite("dosido_si_filtered");

    wavWrite(reconstructedDoSiDo, "output/dosido_filtered.wav");

    // Fft again
    spectrum = fft(reconstructedDoSiDo);
    normSpectrum = normalize(abs(fftShift(spectrum)));

    fftPoints = points;

    plotTitle("Si-filtered DoSiDo Spectrum 2");
    plotLabels("Frequency (Hz)", "Percentage");
    plot(fftPoints, normSpectrum);
    plotArea(-600, 600, 0, 0);
    plotWrite("si_filtered_dosido_spectrum_2");
}

void filterUsingButter()
{
    const double ampl = 256;
    const double fs = SAMPL_FREQ_44100;

    DoubleVector data;
    genSine(data, music::DO, ampl, fs, 131072);
    genSine(data, music::SI, ampl, fs, 65536);
    genSine(data, music::DO, ampl, fs, 65536);

    std::size_t N = data.size();

    DoubleVector points = linSpace(0, N, N);

    plotTitle("DoSiDo signal");
    plotLabels("Samples", "Amplitude");
    plot(points, data);
    plotWrite("dosido");

    DoubleVector laFiltered = butter(data, music::DO, fs);

    plotTitle("Filtering Si using Butterworth");
    plotLabels("Samples", "Amplitude");
    plot(points, laFiltered);
    plotWrite("dosido_si_butter");

    wavWrite(laFiltered, "output/dosido_si_butter.wav");
}

int main(int argc, char **argv)
{
    plotDirectory("output");

    genLa();
    genLaSpectrum();
    genLaScaleWithFourier();
    plotWaveFile();
    filterUsingDft();
    filterUsingButter();

	return 0;
}
