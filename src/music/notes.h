#ifndef MUSIC_NOTES_H
#define MUSIC_NOTES_H

#include <vector>

namespace music {

//french notation here ; TODO provide aliases with english notation
constexpr const double DO = 261.63;
constexpr const double DO_DIESE = 277.18;
constexpr const double RE = 293.66;
constexpr const double RE_DIESE = 311.13;
constexpr const double MI = 329.63;
constexpr const double FA = 349.23;
constexpr const double FA_DIESE = 369.99;
constexpr const double SOL = 392.;
constexpr const double SOL_DIESE = 415.30;
constexpr const double LA = 440.;
constexpr const double LA_DIESE = 466.16;
constexpr const double SI = 493.88;

std::vector<double> notes();
std::vector<double> notesWithSemi();

std::vector<double> chromaticScale(const double from);
std::vector<double> chromaticScaleWithSemi(const double from);

} // namespace music

#endif // MUSIC_NOTES_H
