#include "notes.h"

#include <algorithm>
#include <array>

namespace {

using namespace music;

constexpr const std::array<const double, 7> NOTES =
    { DO, RE, MI, FA, SOL, LA, SI };

constexpr const std::array<const double, 12> NOTES_WITH_SEMI =
    { DO, DO_DIESE, RE, RE_DIESE, MI, FA, FA_DIESE, SOL, SOL_DIESE, LA,
      LA_DIESE, SI};

template <typename T>
std::vector<T> rotatedVector(const std::vector<T> &src, const T &from)
{
    auto pivotIt = std::find(src.begin(), src.end(), from);

    std::vector<T> ret(src.size());

    std::rotate_copy(src.begin(), pivotIt, src.end(), ret.begin());

    return ret;
}

} // anon namespace

namespace music {

std::vector<double> notes()
{
    return std::vector<double>(NOTES.begin(), NOTES.end());
}

std::vector<double> notesWithSemi()
{
    return std::vector<double>(NOTES_WITH_SEMI.begin(), NOTES_WITH_SEMI.end());
}

std::vector<double> chromaticScale(const double from)
{
    return rotatedVector(notes(), from);
}

std::vector<double> chromaticScaleWithSemi(const double from)
{
    return rotatedVector(notesWithSemi(), from);
}

} // namespace music
