# !/bin/bash

generatedPath="../build/generated"

mkdir -p $generatedPath

cd $generatedPath
cmake ../../source
make

if [ $? -eq 0 ] && [ $# -eq 1 ]; then
	cd ../install

	echo "Starting $1"

	./$1
fi
